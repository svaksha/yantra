# yantra

**Standard Disclaimer**: No warranty/guarantee for any particular use, express or implied and you agree to use [these program files](http://svaksha.github.io/yantra) at your own risk and liability!

# LICENSE
* COPYRIGHT© 2007-Now [SVAKSHA](http://svaksha.com/pages/Bio) AllRightsReserved.
+ This work is licensed and distributed under the [AGPLv3 License](http://www.gnu.org/licenses/agpl.html) and ALL software copies and forks of this work must retain the Copyright, Licence (and the LICENSE.md file) and this permission notice.
